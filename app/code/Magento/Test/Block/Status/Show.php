<?php
/**
 * Show
 *
 * @copyright Copyright © 2021 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magento\Test\Block\Status;


use Magento\Framework\View\Element\Template;

class Show extends Template
{

    private $_cusomer_session;
    public function __construct(
        \Magento\Customer\Model\Session  $cusomtersession,
        Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->_cusomer_session=$cusomtersession;
    }



    public function isLoggedIn(): bool
    {

        return $this->_cusomer_session->isLoggedIn();
    }

    /**
     * @return string
     */
    public function getCustomerStatus(){
        return $this->_cusomer_session->getCustomer()->getData('customer_status');
    }
}
